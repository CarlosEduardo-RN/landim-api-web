'use-strict';
var repository = require('../repository/tb_locais_risco');
var models = require('../models');
var log = require('../repository/log');

exports.save = async (req, res, next) => {
    const response = await repository.save(req.body);
    response ? res.status(200).json({ msg: "Local de risco criado com sucesso!", status: 200 }) : res.status(400).json({ msg: "Erro ao criar Local de risco", status: 400 });
}

exports.getTipos = async (req, res, next) => {
    const response = await repository.get(req.body, req.params);
    response ? res.status(200).json(response) : res.status(400).json({ msg: "Erro ao buscar Locais de risco", status: 400 });
}

exports.atualizar = async (req, res, next) => {
    const response = await repository.update(req.body, req.params);
    response ? res.status(200).json({ msg: "Local de risco atualizado com sucesso", status: 200 }) : res.status(400).json({ msg: "Erro ao atualizar Local de risco", status: 400 });
}

exports.deletar = async (req, res, next) => {
    const response = await repository.delete(req.params);
    response ? res.status(200).json({ msg: "Local de risco deletado com sucesso", status: 200 }) : res.status(400).json({ msg: "Erro ao deletar Local de risco", status: 400 });

}