'use-strict';
var repository = require('../repository/tb_cobertura_minima');
var models = require('../models');
var log = require('../repository/log');

exports.save = async (req, res, next) => {
    const response = await repository.save(req.body);
    response ? res.status(200).json({ msg: "Cobertura mininma criada com sucesso!", status: 200 }) : res.status(400).json({ msg: "Erro ao criar Cobertura mininma", status: 400 });
}

exports.getTipos = async (req, res, next) => {
    const response = await repository.get(req.body, req.params);
    response ? res.status(200).json(response) : res.status(400).json({ msg: "Erro ao buscar coberturas minimas", status: 400 });
}

exports.atualizar = async (req, res, next) => {
    const response = await repository.update(req.body, req.params);
    response ? res.status(200).json({ msg: "Cobertura mininma atualizada com sucesso", status: 200 }) : res.status(400).json({ msg: "Erro ao atualizar Cobertura mininma", status: 400 });
}

exports.deletar = async (req, res, next) => {
    const response = await repository.delete(req.params);
    response ? res.status(200).json({ msg: "Cobertura mininma deletada com sucesso", status: 200 }) : res.status(400).json({ msg: "Erro ao deletar Cobertura mininma", status: 400 });

}