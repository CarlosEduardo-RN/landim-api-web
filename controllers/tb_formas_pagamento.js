'use-strict';
var repository = require('../repository/tb_formas_pagamento');
var models = require('../models');
var log = require('../repository/log');

exports.save = async (req, res, next) => {
    const response = await repository.save(req.body);
    response ? res.status(200).json({ msg: "Forma de pagamento criada com sucesso!", status: 200 }) : res.status(400).json({ msg: "Erro ao criar forma de pagamento", status: 400 });
}

exports.getFormas = async (req, res, next) => {
    const response = await repository.get(req.body, req.params);
    response ? res.status(200).json(response) : res.status(400).json({ msg: "Erro ao buscar forma de pagamento", status: 400 });
}

exports.atualizar = async (req, res, next) => {
    const response = await repository.update(req.body, req.params);
    response ? res.status(200).json({ msg: "Forma de pagamento atualizada com sucesso", status: 200 }) : res.status(400).json({ msg: "Erro ao atualizar forma de pagamento", status: 400 });
}

exports.deletar = async (req, res, next) => {
    const response = await repository.delete(req.params);
    response ? res.status(200).json({ msg: "Forma de pagamento deletada com sucesso", status: 200 }) : res.status(400).json({ msg: "Erro ao deletar forma de pagamento", status: 400 });

}