'use-strict';
const nodemailer = require('nodemailer');
const path = require('path');

var Utils = {

    enviarEmail: (titulo, text) => {
        const transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 465,
            secure: true, // true for 465, false for other ports
            auth: {
                user: "gamewriter.noreply@gmail.com",
                pass: "maximuss100"
            },
            tls: { rejectUnauthorized: false }
        });
        const mailOptions = {
            from: 'gamewriter.noreply@gmail.com',
            to: 'alesonmorais@gmail.com',
            subject: titulo,
            html: text
        };
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log('Email enviado: ' + info.response);
            }
        });

    },

    enviarEmailCliente: (titulo, text, email) => {
        const transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 465,
            secure: true, // true for 465, false for other ports
            auth: {
                user: "noreplylandimcorretora@gmail.com",
                pass: "l3p8U6@%oI8w "
            },
            tls: { rejectUnauthorized: false }
        });
        const mailOptions = {
            from: 'noreplylandim@gmail.com',
            to: email,
            subject: titulo,
            html: text,
            attachments: [{
                filename: 'logo.png',
                path: path.join(__dirname + '/assets/WP.png'),
                cid: 'logo'
            }]
        };
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log('Email enviado: ' + info.response);
            }
        });

    }

}

module.exports = Utils;
