'use-strict';
var models = require('../models');
const bcrypt = require('bcrypt');
const fs = require('fs');
const Op = models.sequelize.Op;
var cfg = require("../config/config-jwt");
var jwt = require("jwt-simple");

var PagamentoRepository = {

    save: async (body) => {
        return models.tb_pagamento.create(body).then(result => { return result });
    },
    get: async (body, params) => {
        return models.tb_pagamento.find({ where: { id: params.id } }).then(result => { return result });
    },
    update: async (body, params) => {
        return models.tb_pagamento.update(body, { whre: { id: params.id } }).then(result => { return result });
    },
    delete: async (params) => {
        return models.tb_pagamento.destroy({ whre: { id: params.id } }).then(result => { return result });
    }

}

module.exports = PagamentoRepository;
