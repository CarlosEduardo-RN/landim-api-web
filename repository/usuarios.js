'use-strict';
var models = require('../models');
const bcrypt = require('bcrypt');
const fs = require('fs');
const Op = models.sequelize.Op;
var cfg = require("../config/config-jwt");
var jwt = require("jwt-simple");

var UsuarioRepository = {

    usuarios: () => {
        return models.users.find({ order: [['id', 'ASC']], attributes: { exclude: ['password'] } }).then(async (result) => { return result; });
    },

    buscarUsuario: (nu_cpf_cnpj) => {
        return models.users.find({ where: { nu_cpf_cnpj: nu_cpf_cnpj } }, { attributes: { exclude: ['password'] } }).then(async (result) => { return result; });
    },

    listar: async (body, headers) => {
        // var tokens = headers.authorization.slice(7);
        // var tokenDecoded = jwt.decode(tokens, cfg.jwtSecret);

        // const res = await models.tb_n_empresa_user.findAndCountAll({ where: { id_empresa: tokenDecoded.minhas_empresas } });
        // var idUsr = await res['rows'].map(e => { return e.dataValues.id_user });

        // var limit = body.limit;
        // var offset = 0 + (body.page - 1) * limit;

        const { tx_nome, nu_cpf_cnpj, tx_tipo } = body;
        const where = {};
        // where.id = { [Op.in]: idUsr };

        if (tx_nome) where.tx_nome = { [Op.like]: `%${tx_nome}%` };
        if (nu_cpf_cnpj) where.nu_cpf_cnpj = nu_cpf_cnpj
        if (tx_tipo) where.tx_tipo = tx_tipo;

        // if (tokenDecoded.length > 0) {
            return models.users.findAndCountAll({
                attributes: { exclude: ['password'] },
                limit: limit,
                offset: offset,
                where
            }).then(async (result) => { return result; })
        // } else {
        //     return models.users.findAndCountAll({
        //         where: { id: tokenDecoded.id }
        //     }).then(async (result) => { return result; })
        // }
    },

    saveUser: (body) => {
        body.password = bcrypt.hashSync(body.password, 10);
        return models.users.create(body).then(function (result) { return result; });
    },

    atualizaUsuario: (body) => {
        if (body.password != null && body.password.length > 0) { body.password = bcrypt.hashSync(body.password, 10); }
        return models.users.update(body, { where: { id: body.id } }).then(function (result) { return result; });
    },

    pesquisarUsuario: (body) => {
        return models.users.findOne({ where: { nu_cpf_cnpj: body.nu_cpf_cnpj } }).then(function (result) {
            if (result != null && result != undefined && result.image != null && result.image.length > 0) {
                let caminho = "public/fotos-usuarios/" + result.id;
                if (fs.existsSync(caminho + '/' + result.image)) {
                    let imagem = fs.readFileSync(caminho + '/' + result.image, (err, data) => {
                        if (err) { }
                        return data;
                    });
                    result.image = imagem.toString();
                }
            }
            return result;
        });
    },

    pesquisarUsuarioSemImagem: (body) => {
        return models.users.findOne({ where: { nu_cpf_cnpj: body.nu_cpf_cnpj } }).then(function (result) { return result; });
    },

    getImageUser: (caminho) => {
        return fs.readFileSync(caminho, (err, data) => {
            if (err) { }
            return data;
        });
    }

}

module.exports = UsuarioRepository;
