'use strict';

module.exports = (sequelize, DataTypes) => {
  const TipoSeguro = sequelize.define('tb_tipo_seguro', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tx_nome: DataTypes.STRING,
    tx_descricao: DataTypes.STRING,
    tx_imagem: DataTypes.STRING,
  }, {
    timestamps: true
  });


  TipoSeguro.associate = function (models) {
    models.tb_tipo_seguro.belongsToMany(models.tb_cobertura_minima, {
      foreignKey: "id_tipo_seguro"
      , through: "tb_n_seguro_valores"
      , otherKey: "id_cobertura_minima"
      , as: "TipoSeguroCobertura"
      , timestamps: false
    });

    models.tb_tipo_seguro.hasOne(models.tb_seguro, {
      foreignKey: "id_tipo_seguro"
      , as: "TipoSeguro"
      , timestamps: false
  });
  }

  return TipoSeguro;
}