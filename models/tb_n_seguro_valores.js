'use strict';

module.exports = (sequelize, DataTypes) => {
    const NSeguroValores = sequelize.define('tb_n_seguro_valores', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_tipo_seguro: {
            type: DataTypes.INTEGER,
            foreignKey: true
        },
        id_cobertura_minima: {
            type: DataTypes.INTEGER,
            foreignKey: true
        }
    }, {
        timestamps: false
    });

    return NSeguroValores;
}