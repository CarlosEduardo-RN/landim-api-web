'use strict';

module.exports = (sequelize, DataTypes) => {
  const FormasPagamento = sequelize.define('tb_forma_pagamento', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    tx_nome: DataTypes.STRING,
    tx_descricao: DataTypes.STRING,
    tx_tipo: DataTypes.STRING,
  }, {
    timestamps: true
  });
  

  FormasPagamento.associate = function (models) {
    models.tb_forma_pagamento.hasMany(models.tb_pagamento, {
        foreignKey: "id_forma_pagamento"
        , as: "FormaPagamento"
        , timestamps: false
    });

}

  return FormasPagamento;
}