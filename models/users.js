'use strict';

module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define('users', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    nu_cpf_cnpj: DataTypes.STRING,
    tx_nome: DataTypes.STRING,
    nu_telefone: DataTypes.STRING,
    dt_nascimento: DataTypes.STRING,
    tx_estado_civil: DataTypes.STRING,
    tx_sexo: DataTypes.STRING,
    tx_logradouro: DataTypes.STRING,
    tx_cidade: DataTypes.STRING,
    tx_bairro: DataTypes.STRING,
    nu_cep: DataTypes.STRING,
    nu_numero: DataTypes.STRING,
    tx_faixa_salarial: DataTypes.DECIMAL(10, 2),
    tx_estrangeiro: DataTypes.STRING,
    tx_tipo: DataTypes.STRING,
    tx_imagem: DataTypes.STRING,
    password: DataTypes.STRING,
    email: DataTypes.STRING
  }, {
    timestamps: true
  });

  Users.associate = function (models) {
    models.users.hasMany(models.tb_locais_risco, {
      foreignKey: "id_user"
      , as: "UserLocal"
      , timestamps: false
    });
    models.users.hasMany(models.tb_seguro, {
      foreignKey: "id_usuario"
      , as: "UserSeguro"
      , timestamps: false
  });
  }

  return Users;
}