'use strict';

module.exports = (sequelize, DataTypes) => {
    const LocaisRisco = sequelize.define('tb_locais_risco', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_user: {
            type: DataTypes.INTEGER,
            foreignKey: true
        },
        tx_logradouro: DataTypes.STRING,
        tx_cidade: DataTypes.STRING,
        tx_bairro: DataTypes.STRING,
        nu_cep: DataTypes.STRING,
        nu_numero: DataTypes.INTEGER
    }, {
        timestamps: true
    });

    LocaisRisco.associate = function (models) {
        models.tb_locais_risco.hasOne(models.tb_seguro, {
            foreignKey: "id_local_risco"
            , as: "LocalSeguro"
            , timestamps: false
        });
    }

    return LocaisRisco;
}