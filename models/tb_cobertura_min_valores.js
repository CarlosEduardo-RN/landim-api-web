'use strict';

module.exports = (sequelize, DataTypes) => {
    const CoberturaMinimaValores = sequelize.define('tb_cobertura_min_valores', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_cobertura_minima: {
            type: DataTypes.INTEGER,
            foreignKey: true,
        },
        tx_nome: DataTypes.STRING,
        vr_valor: DataTypes.DECIMAL(10, 2),
    }, {
        timestamps: true
    });


    return CoberturaMinimaValores;
}