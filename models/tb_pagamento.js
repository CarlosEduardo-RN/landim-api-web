'use strict';

module.exports = (sequelize, DataTypes) => {
    const Pagamento = sequelize.define('tb_pagamento', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_forma_pagamento: {
            type: DataTypes.INTEGER,
            foreignKey: true
        },
        dt_pagamento: DataTypes.DATE,
        vr_pago: DataTypes.DECIMAL(10, 2),
    }, {
        timestamps: true
    });

    return Pagamento;
}