'use strict';

module.exports = (sequelize, DataTypes) => {
    const CoberturaMinima = sequelize.define('tb_cobertura_minima', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        tx_faixa_cobertura: DataTypes.STRING,
        tx_nome: DataTypes.STRING
    }, {
        timestamps: true
    });

    CoberturaMinima.associate = function (models) {
        models.tb_cobertura_minima.belongsToMany(models.tb_tipo_seguro, {
            foreignKey: "id_cobertura_minima"
            , through: "tb_n_seguro_valores"
            , otherKey: "id_tipo_seguro"
            , as: "CoberturaSeguro"
            , timestamps: false
        });

        models.tb_cobertura_minima.hasMany(models.tb_cobertura_min_valores, {
            foreignKey: "id_cobertura_minima"
            , as: "CoberturaValores"
            , timestamps: false
        });

    }
    return CoberturaMinima;
}