'use strict';

module.exports = (sequelize, DataTypes) => {
    const Seguro = sequelize.define('tb_seguro', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_parceiro: {
            type: DataTypes.INTEGER,
            foreignKey: true,
            allowNull: true
        },
        id_usuario: {
            type: DataTypes.INTEGER,
            foreignKey: true
        },
        id_local_risco: {
            type: DataTypes.INTEGER,
            foreignKey: true,
        },
        id_tipo_seguro: {
            type: DataTypes.INTEGER,
            foreignKey: true
        },
        dt_vencimento: DataTypes.DATE,
        vr_seguro: DataTypes.DECIMAL(10, 2),
    }, {
        timestamps: true
    });

    return Seguro;
}