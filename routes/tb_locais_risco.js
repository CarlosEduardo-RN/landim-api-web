var express = require('express');
var router = express.Router();
var controller = require('../controllers/tb_locais_risco');
var auth = require("../middlewares/auth-jwt")();

router.post('/create', auth.authenticate(), controller.save);
router.get('/list/:id', auth.authenticate(), controller.getTipos);
router.put('/update/:id', auth.authenticate(), controller.atualizar);
router.delete('/delete/:id', auth.authenticate(), controller.deletar);

module.exports = router;
