var express = require('express');
var router = express.Router();
var controller = require('../controllers/usuarios');
var auth = require("../middlewares/auth-jwt")();

router.post('/get', auth.authenticate(), controller.find);
router.post('/verificacpf', controller.verificaCPF);
router.put('/atualiza', auth.authenticate(), controller.atualizar);
router.delete('/:id', auth.authenticate(), controller.deletar);
router.post('/lista', auth.authenticate(), controller.listarUsuario);
router.post('/', auth.authenticate(), controller.save);

module.exports = router;
