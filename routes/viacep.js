var express = require('express');
var router = express.Router();
var controller = require('../controllers/viacepController');
var auth = require("../middlewares/auth-jwt")();

router.post('/', auth.authenticate(), controller.buscarcepjson);

module.exports = router;
